#pragma once
enum class TextureID
{
	Entities,
	Jungle,
	TitleScreen,
	Buttons,
	Explosion,
	Particle,
	FinishLine
};